/** Dependencies */
import { NodeBlock } from "tripetto-runner-foundation";

export interface IParagraph {
    readonly caption?: string;
    readonly imageURL?: string;
    readonly imageWidth?: string;
    readonly imageAboveText?: boolean;
    readonly video?: string;
}

export abstract class Paragraph extends NodeBlock<IParagraph> {}
