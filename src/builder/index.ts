/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

import {
    Forms,
    NodeBlock,
    REGEX_IS_URL,
    definition,
    editor,
    isString,
    pgettext,
    slotInsertAction,
    tripetto,
} from "tripetto";
import { IParagraph } from "../runner";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    alias: "paragraph",
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:paragraph", "Paragraph");
    },
})
export class Paragraph extends NodeBlock implements IParagraph {
    @definition
    caption?: string;

    @definition
    imageURL?: string;

    @definition
    imageAboveText?: boolean;

    @definition
    imageWidth?: string;

    @definition
    video?: string;

    private static getYouTubeId(url: string): string {
        const videoID = url.match(
            /youtu(?:.*\/v\/|.*v\=|\.be\/)([A-Za-z0-9_\-]{11})/
        );

        return (videoID && videoID.length === 2 && videoID[1]) || "";
    }

    private static getVimeoId(url: string): string {
        const videoID = url.match(
            /\/\/(?:www\.)?vimeo\.com\/(?:channels\/staffpicks\/)?([-\w]+)/i
        );

        return (videoID && videoID.length === 2 && videoID[1]) || "";
    }

    @editor
    defineEditor(): void {
        this.editor.name(true, true);
        this.editor.option({
            name: pgettext("block:paragraph", "Caption"),
            form: {
                title: pgettext("block:paragraph", "Caption"),
                controls: [
                    new Forms.Text(
                        "multiline",
                        Forms.Text.bind(this, "caption", undefined)
                    )
                        .placeholder(
                            pgettext(
                                "block:paragraph",
                                "Type caption text here..."
                            )
                        )
                        .action("@", slotInsertAction(this)),
                ],
            },
            activated: isString(this.caption),
        });
        this.editor.description();

        this.editor.option({
            name: pgettext("block:paragraph", "Image"),
            form: {
                title: pgettext("block:paragraph", "Image"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "imageURL", undefined)
                    )
                        .label(pgettext("block:paragraph", "Image source URL"))
                        .inputMode("url")
                        .placeholder("https://")
                        .autoValidate((ref: Forms.Text) =>
                            ref.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(ref.value) ||
                                  (ref.value.length > 23 &&
                                      ref.value.indexOf(
                                          "data:image/jpeg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/png;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/svg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/gif;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 1 &&
                                      ref.value.charAt(0) === "/")
                                ? "pass"
                                : "fail"
                        ),
                    new Forms.Text(
                        "singleline",
                        Forms.Checkbox.bind(this, "imageWidth", undefined)
                    )
                        .label(
                            pgettext(
                                "block:paragraph",
                                "Image width (optional)"
                            )
                        )
                        .width(100)
                        .align("center"),
                    new Forms.Checkbox(
                        pgettext(
                            "block:paragraph",
                            "Display image on top of the paragraph"
                        ),
                        Forms.Checkbox.bind(this, "imageAboveText", undefined)
                    ),
                ],
            },
            activated: isString(this.imageURL),
        });

        this.editor.option({
            name: pgettext("block:paragraph", "Video"),
            form: {
                title: pgettext("block:paragraph", "Video"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "video", undefined)
                    )
                        .inputMode("url")
                        .placeholder("https://")
                        .autoValidate((video: Forms.Text) =>
                            video.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(video.value) &&
                                  (Paragraph.getYouTubeId(video.value) ||
                                      Paragraph.getVimeoId(video.value))
                                ? "pass"
                                : "fail"
                        ),
                    new Forms.Static(
                        pgettext(
                            "block:paragraph",
                            "Currently [YouTube](https://youtube.com) and [Vimeo](https://vimeo.com) videos are supported. Just click the share button of your video and paste the resulting embed link in the field above."
                        )
                    ).markdown(),
                ],
            },
            activated: isString(this.video),
        });

        this.editor.explanation();

        this.editor.groups.options();
        this.editor.visibility();
    }
}
